<?php

?>
<div id="page-wrapper">
  <div id="page">

    <div id="header" class="<?php print $secondary_menu ? 'with-secondary-menu' : 'without-secondary-menu'; ?>">
      <div class="section clearfix">
        <?php print render($page['header']); ?>
      </div> <!-- /.section -->
    </div> <!-- /#header -->

    <?php if ($page['featured']): ?>
      <div id="featured">
        <div class="section clearfix">
          <?php print render($page['featured']); ?>
        </div> <!-- /.section -->
      </div> <!-- /#featured -->
    <?php endif; ?>

    <div id="main-wrapper" class="clearfix">
      <div id="main" class="clearfix">
        <div id="content" class="column">
          <div class="section">
            <?php if ($page['highlighted']): ?>
              <div id="highlighted">
                <?php print render($page['highlighted']); ?>
              </div>
            <?php endif; ?>
            <a id="main-content"></a>
            <?php print render($page['content']); ?>
          </div> <!-- /.section -->
        </div> <!-- /#content -->

        <?php if ($page['sidebar_second']): ?>
          <div id="sidebar-second" class="column sidebar">
            <div class="section">
              <?php print render($page['sidebar_second']); ?>
            </div> <!-- /.section -->
          </div> <!-- /#sidebar-second -->
        <?php endif; ?>
      </div> <!-- /#main -->
    </div> <!-- /#main-wrapper -->

    <?php if ($page['triptych_first'] || $page['triptych_last']): ?>
      <div id="triptych-wrapper">
        <div id="triptych" class="clearfix">
          <?php print render($page['triptych_first']); ?>
          <?php print render($page['triptych_last']); ?>
        </div> <!-- /#triptych -->
      </div> <!-- /#triptych-wrapper -->
    <?php endif; ?>

    <div id="footer-wrapper">
      <div class="section">
        <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn']): ?>
          <div id="footer-columns" class="clearfix">
            <?php print render($page['footer_firstcolumn']); ?>
            <?php print render($page['footer_secondcolumn']); ?>
          </div> <!-- /#footer-columns -->
        <?php endif; ?>

      <?php if ($page['footer']): ?>
        <div id="footer" class="clearfix">
          <?php print render($page['footer']); ?>
        </div> <!-- /#footer -->
      <?php endif; ?>

      </div> <!-- /.section -->
    </div> <!-- /#footer-wrapper -->

  </div> <!-- /#page -->
</div> <!-- /#page-wrapper -->
